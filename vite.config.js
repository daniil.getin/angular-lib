const { resolve } = require('path');
import { defineConfig } from 'vite';
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js';

const config = {
  build: {
    lib: {
      entry: resolve(__dirname, 'angular/index.js'),
      name: 'angular-lib',
      fileName: 'angular-lib',
      formats: ['es', 'cjs']
    },
    outDir: resolve(__dirname, 'dist'),
    sourcemap: true,
    emptyOutDir: true
  },
  plugins: [
    cssInjectedByJsPlugin()
  ]
};

export default defineConfig(config);
