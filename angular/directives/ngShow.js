export default function ngShow() {
  return {
    link: (node, scope) => {
      const expression = node.getAttribute('ng-show');
      const toggleVisibility = () => node.classList.toggle('ng_invise', !scope.eval(expression));

      toggleVisibility();
      scope.$watch(expression, toggleVisibility);
    }
  };
}
