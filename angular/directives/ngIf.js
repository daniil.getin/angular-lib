export default function ngIf() {
  return {
    link: (node, scope) => {
      const expression = node.getAttribute('ng-if');

      const toggleElement = () => {
        node.hidden = !scope.eval(expression);
      };

      toggleElement();
      scope.$watch(expression, toggleElement);
    }
  };
}
