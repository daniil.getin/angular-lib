export default function ngRepeat() {
  return {
    link: (node, scope) => {
      const [variableName, dataset] = node.getAttribute('ng-repeat').split(' in ');
      const { parentNode: parent } = node;

      node.removeAttribute('ng-repeat');
      node.classList.add('ng-repeated');
      node.remove();

      const repeatHandler = () => {
        const fragment = document.createDocumentFragment();

        parent.querySelectorAll('.ng-repeated').forEach(node => node.remove());

        for (const key of scope.eval(dataset)) {
          const item = node.cloneNode(true);

          scope[variableName] = key;
          item.innerHTML = item.innerHTML.replace(/{{(.*?)}}/g, (_, str) => scope.$$parser(scope, str));
          fragment.append(item);
          item.querySelectorAll('*').forEach(scope.$$compile);
        }
        parent.append(fragment);
      };

      repeatHandler();
      scope.$watch(variableName, repeatHandler);
    }
  };
}
