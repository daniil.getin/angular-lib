export default function ngModel() {
  return {
    link: (node, scope, attributes) => {
      const variable = node.getAttribute('ng-model');
      const type = attributes.type === 'checkbox' ? 'checked' : 'value';
      const setVariable = () => {
        if (type === 'value') {
          scope.eval(`${variable} = '${node[type]}'`);
          return;
        }
        scope.eval(`${variable} = ${node[type]}`);
      };

      const setInputValue = () => {
        node[type] = scope.eval(variable);
      };

      if (!String(scope[variable])) {
        setVariable();
      }

      setInputValue();
      scope.$watch(variable, setInputValue);

      node.addEventListener('input', () => {
        setVariable();
        scope.$apply();
      });
    }
  };
}
