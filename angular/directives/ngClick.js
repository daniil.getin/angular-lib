export default function ngClick() {
  return {
    link: (node, scope) => {
      const expression = node.getAttribute('ng-click');

      node.addEventListener('click', () => {
        scope.eval(expression);
        scope.$apply();
      });
    }
  };
}
