export default function ngSrc() {
  return {
    link: (node, scope) => {
      const variable = node.getAttribute('ng-src');

      const setUrl = () => {
        const imageUrl = scope.eval(variable);

        if (!imageUrl) {
          return;
        }

        node.src = imageUrl;
      };

      setUrl();
      scope.$watch(variable, setUrl);
    }
  };
}
