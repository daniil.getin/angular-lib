export default function ngFile() {
  return {
    link: (node, scope) => {
      const [controllerName, variable] = node.getAttribute('ng-file').split('.');

      node.addEventListener('change', ({ target: { files: [file] } }) => {
        if (file) {
          scope[controllerName][variable] = file;
          scope[controllerName].fileName = file.name;
        }

        scope.$apply();
      });
    }
  };
}
