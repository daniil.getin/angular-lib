export default function ngInit() {
  return {
    link: (node, scope) => {
      scope.eval(node.getAttribute('ng-init'));
      scope.$apply();
    }
  };
}
