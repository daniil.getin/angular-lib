export default function ngBlur() {
  return {
    link: (node, scope) => {
      const expression = node.getAttribute('ng-blur');
      const toggleBlur = () => {
        node.style.filter = `blur(${scope.eval(expression)}px)`;
      };

      toggleBlur();
      scope.$watch(expression, toggleBlur);
      scope.$apply();
    }
  };
}
