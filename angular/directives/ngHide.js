export default function ngHide() {
  return {
    link: (node, scope) => {
      const expression = node.getAttribute('ng-hide');
      const toggleVisibility = () => node.classList.toggle('ng_invise', scope.eval(expression));

      toggleVisibility();
      scope.$watch(expression, toggleVisibility);
    }
  };
}
