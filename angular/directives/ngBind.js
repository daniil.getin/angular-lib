export default function ngBind() {
  return {
    link: (node, scope) => {
      const variable = node.getAttribute('ng-bind');

      const setNodeValue = () => {
        node.textContent = scope.eval(variable);
      };

      setNodeValue();
      scope.$watch(variable, setNodeValue);
    }
  };
}
