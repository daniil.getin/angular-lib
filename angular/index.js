import * as utils from './utils/index.js';
import * as defaultDirectives from './directives/index.js';
import * as defaultFilters from './filters/index.js';
import * as defaultServices from './services/index.js';
import './style.css';

function SmallAngular() {
  const rootScope = window;
  const directives = {};
  const watchers = {};
  const controllers = {};
  const components = {};
  const services = {};
  const filters = {};
  const configs = [];


  function directive(name, fn) {
    directives[name] = fn;

    return this;
  }

  function component(componentName, func) {
    components[utils.toCamelCase(componentName)] = func;

    return this;
  }

  function getDependencies(names) {
    return names.map(name => {
      if (name === '$rootScope' || name === '$scope') {
        return rootScope;
      }

      if (!services[name]) {
        throw new Error(`${name} service isn't registered!`);
      }

      return services[name];
    });
  }

  function service(name, fn) {
    const funcWithDeps = utils.setDependencies(fn);
    const funcArgs = getDependencies(funcWithDeps.dependencies);

    services[name] = funcWithDeps(...funcArgs);

    return this;
  }

  function constant(name, value) {
    services[name] = value;

    return this;
  }

  function config(fn) {
    configs.push(utils.setDependencies(fn));

    return this;
  }

  function controller(controllerName, func) {
    controllers[controllerName] = utils.setDependencies(func);

    return this;
  }

  const compile = node => {
    const {
      angularAttributes,
      commonAttributes
    } = utils.getAllSplitedAtributes(node, directives);
    const tag = utils.toCamelCase(node.tagName.toLowerCase());
    const component = components[tag];

    if (component) {
      const { template, link, controller, controllerAs } = component();

      if (template) {
        node.innerHTML = template;
      }

      const Controller = controllers[controller];

      if (!Controller && link) {
        link(node, rootScope, commonAttributes);
      }

      if (!Controller) {
        throw new Error(`${controller} isn't registered!`);
      }

      const controllerDependencies = getDependencies(Controller.dependencies);
      const controllerInstance = new Controller(...controllerDependencies);

      if (controllerAs) {
        rootScope[controllerAs] = controllerInstance;
      }

      node.querySelectorAll('*').forEach(compile);
    }

    for (const key in angularAttributes) {
      directives[key]().link(node, rootScope, commonAttributes);
    }
  };

  rootScope.$$compile = compile;
  rootScope.$$parser = (scope, str) => {
    const [
      variable,
      ...filtersArray
    ] = str.split('|').map(item => item.trim());

    return filtersArray.reduce((current, filter) => filters[filter](current), scope.eval(variable));
  };

  rootScope.$apply = () => {
    for (const name in watchers) {
      for (const callback of watchers[name]) {
        callback();
      }
    }
  };

  rootScope.$applyAsync = () => setTimeout(rootScope.$apply);

  this.module = appName => {
    this.appName = appName;

    return this;
  };

  rootScope.$watch = (name, callback) => {
    if (!watchers[name]) {
      watchers[name] = [];
    }

    watchers[name].push(callback);
  };

  this.bootstrap = (appNode = document.querySelector('[ng-app]')) => {
    if (!appNode) {
      return null;
    }

    const nodes = appNode.querySelectorAll('*');

    compile(appNode);

    for (const node of nodes) {
      compile(node);
    }
  };

  this.directive = directive;
  this.controller = controller;
  this.component = component;
  this.service = service;
  this.constant = constant;
  this.config = config;

  Object.assign(directives, defaultDirectives);
  Object.assign(filters, defaultFilters);

  const initDefaultServices = () => {
    for (const key in defaultServices) {
      service(key, defaultServices[key]);
    }
  };

  const initDefaultConfigs = () => {
    configs.forEach(configFn => {
      const functionArguments = getDependencies(configFn.dependencies);

      configFn(...functionArguments);
    });
  };

  initDefaultServices();

  setTimeout(() => {
    initDefaultConfigs();
    this.bootstrap();
  });
}

window.angular = new SmallAngular();
export default window.angular;
