export default function $timeout($rootScope) {
  return function(callback, delay) {
    setTimeout(() => {
      $rootScope.$applyAsync();
      callback();
    }, delay);
  };
}
$timeout.dependencies = ['$rootScope'];
