export default function $http($rootScope) {
  const REQUEST_METHODS = {
    GET: 'GET',
    POST: 'POST',
    DELETE: 'DELETE'
  };
  const RESPONSE_TYPE_JSON = 'json';

  class HttpRequest {
    constructor(url) {
      this.url = url;
    }

    get(url, config = {}) {
      return this.request({ ...config, url, method: REQUEST_METHODS.GET });
    }

    post(url, data, config = {}) {
      return this.request({ ...config, url, data, method: REQUEST_METHODS.POST });
    }

    delete(url, config = {}) {
      return this.request({ ...config, url, method: REQUEST_METHODS.DELETE });
    }

    request(config) {
      const {
        url,
        data,
        method,
        responseType = RESPONSE_TYPE_JSON,
        headers = {}
      } = config;
      const xhr = new XMLHttpRequest();

      const fullUrl = new URL(url, this.url);

      xhr.open(method, fullUrl);
      xhr.responseType = responseType;

      for (const key in headers) {
        xhr.setRequestHeader(key, headers[key]);
      }

      xhr.send(data || null);

      return new Promise((resolve, reject) => {
        xhr.onload = () => {
          const result = xhr;

          if (result.status >= 400) {
            reject(result.status);
          }

          const response = {
            method,
            status: result.status,
            data: result.response,
            url: result.responseURL,
            headers
          };
          $rootScope.$applyAsync();
          resolve(response);
        };
        xhr.onerror = () => reject(xhr);
      });
    }
  }
  return new HttpRequest();
}
$http.dependencies = ['$rootScope'];
