export default function toCamelCase(string) {
  if (typeof string !== 'string') {
    throw new TypeError();
  }

  return string
    .split(/[_-]/g)
    .map((element, index) => index === 0 ? element : `${element[0].toUpperCase()}${element.slice(1)}`)
    .join('');
}
