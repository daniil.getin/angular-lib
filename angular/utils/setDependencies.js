function isInjection(func) {
  return (/{\n\s+('inject')/).test(func.toString());
}

function extractFunctionArguments(func) {
  const args = func.toString().match(/\((.*)\)/)[1];

  return args.split(/\s*,\s*/);
}

export default function setDependencies(args) {
  let fn = args;

  if (Array.isArray(args)) {
    fn = args.pop();
    fn.dependencies = args;
  } else if (isInjection(fn)) {
    fn = extractFunctionArguments(fn);
  }

  if (!fn.dependencies) {
    fn.dependencies = [];
  }

  return fn;
}
