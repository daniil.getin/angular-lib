import toCamelCase from './toCamelCase.js';

export default function getAllSplitedAtributes({ attributes }, directives) {
  const allAttributes = {
    angularAttributes: {},
    commonAttributes: {}
  };

  for (const { name, value } of attributes) {
    const directiveName = toCamelCase(name);
    const attribute = directives[directiveName] ? 'angularAttributes' : 'commonAttributes';

    allAttributes[attribute][directiveName] = value;
  }

  return allAttributes;
}
