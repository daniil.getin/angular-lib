
# Angular-lib

Custom implementation of the Angular library


## Installation

#### Install angular with npm

1. Add this file to package.json

```json
  "angular-lib": "gitlab:daniil.getin/angular-lib#v1.0.1" 
```
2. Enter this command for download lib module

```node
  npm install
```
    
## How start to work

```js
import angular from 'angular-lib'; // import the library


const app = angular.module('myApp'); // create 

```
## Standart directives

| Directive | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `ng-model` | `variable` | Binds input, select and textarea, and stores the required user value in a variable|
| `ng-bind` | `variable` | Inserts the text value of the variable into the node|
| `ng-if` | `boolean expression` | Inserts or removes a node depending on the value |
| `ng-show` | `boolean expression` | Inserts or removes a node depending on the value |
| `ng-hide` | `boolean expression` | Inserts or removes a node depending on the value |
| `ng-click` | `expreboolean expressionssion` | Executes the given expression on click |
